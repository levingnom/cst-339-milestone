


CREATE TABLE dbo.User(
	ID int NOT NULL auto_increment,
	FirstName varchar(50) NOT NULL,
	LastName varchar(50) NOT NULL,
	Email varchar(150) NULL,
	Phone varchar(50) NULL,
	LoginName varchar(50) NOT NULL,
	Password varchar(50) NOT NULL,
 CONSTRAINT PK_User PRIMARY KEY CLUSTERED 
(
	ID ASC
)
)
;


CREATE TABLE dbo.Procedure(
	ID int NOT NULL auto_increment,
	nameOfProcedure varchar(50) NOT NULL,
	specialtyArea varchar(50) NOT NULL,
	riskOfFailure int NOT NULL,
	surgeryDay date NOT NULL,
	Photo varchar(50) NULL,
	Description varchar(300) NULL,
	Price numeric(10, 2) NULL,
 CONSTRAINT PK_Procedure PRIMARY KEY CLUSTERED 
(
	ID ASC
)
)
;
